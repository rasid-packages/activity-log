<?php

namespace Rasid\Activity\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Facades\DB;
use Rasid\Activity\Models\Log;

trait Loggable
{
    public static function logToDb($model, $logType) :void
    {
        if (!auth()->check() || !config('activity.activated', true)) return;

        $data = array_merge(self::originalData($model, $logType), self::extraData());

        DB::table(config('activity.table'))->insert($data);
    }

    public static function originalData($model, $logType) :array
    {

        $originalData = $logType === 'create' ? json_encode($model) : json_encode($model->getRawOriginal());

        return [
            'user_id'    => auth()->user()?->id,
            'log_date'   => date('Y-m-d H:i:s'),
            'subject_type' => get_class(),
            'subject_id' => $model->id,
            'ip_address' => request()->ip(),
            'log_type'   => $logType,
            'data'       => $originalData
        ];
    }

    public static function extraData() :array
    {
        return [];
    }

    public static function bootLoggable() :void
    {
        if (config('activity.log_events.on_edit', false)) {
            self::updated(function ($model) {
                self::logToDb($model, 'edit');
            });
        }

        if (config('activity.log_events.on_delete', false)) {
            self::deleted(function ($model) {
                self::logToDb($model, 'delete');
            });
        }

        if (config('activity.log_events.on_create', false)) {
            self::created(function ($model) {
                self::logToDb($model, 'create');
            });
        }
    }

    public function activities() :MorphMany
    {
        return $this->morphMany(Log::class, 'subject');
    }

}
