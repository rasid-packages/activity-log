<?php

namespace Rasid\Activity\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    public $timestamps = false;

    public $dates = ['log_date'];

    protected $appends = ['dateHumanize','json_data'];

    private $userInstance = "App\Models\User";

    public function __construct()
    {
        if(!empty(config('activity.model'))) $this->userInstance = config('activity.model');
    }

    public function getDateHumanizeAttribute()
    {
        return $this->log_date->diffForHumans();
    }

    public function getJsonDataAttribute()
    {
        return json_decode($this->data,true);
    }

    public function user()
    {
        return $this->belongsTo($this->userInstance);
    }

}
